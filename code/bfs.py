import time
import copy

# ------------------------------------------------------------------

trueBoards = []

def fill(board, size) :
    if (size == 8) :
        trueBoards.append(board.copy())
        return

    for poss in range(8) :
        fail = False
        
        for priv in range(size) :
            if (poss == board[priv]) :
                fail = True
                break
            if (abs(poss - board[priv]) == abs(size - priv)) :
                fail = True
                break
        
        if (fail) :
            continue
        
        if(size < len(board)) :
            board[size] = poss
        else :
            board.append(poss)

        fill(board, size+1)

fill([], 0)

# -------------------------------------------------------------------

trueBoard_index = 0
for trueBoard in trueBoards :
    newBoard = [['O' for i in range(8)] for j in range(8)]
    
    for row in range(8) :
        newBoard[row][ trueBoard[row] ] = 'X'
    
    trueBoards[trueBoard_index] = newBoard
    trueBoard_index += 1

# --------------------------------------------------------------------

def print_gird(gird) :
    print('-------------------')
    for line in gird :
        print('| ', end='')
        for square in line :
            print(square+' ', end='')
        print('|')
    print('-------------------')

# ---------------------------------------------------------------------

myBoard = [['O' for i in range(8)] for j in range(8)]

for i in range(8) :
    row,column = tuple(map(int,(input().split(','))))
    myBoard[row-1][column-1] = 'X'

print_gird(myBoard)

dx = [0, 1, 0, -1, 1, 1, -1, -1]
dy = [-1, 0, 1, 0, -1, 1, 1, -1]

# ---------------------------------------------------------------------

def underThreat(board, row, column) :
    for i in range(8) :
        if  i == column :
            continue
        if(board[row][i] == 'X') :
            return True
    for i in range(8) :
        if i == row :
            continue
        if(board[i][column] == 'X') :
            return True
        if(column+row-i >= 0 and column+row-i < 8 and board[i][column+row-i] == 'X') :
            return True
        if(column-row+i >= 0 and column-row+i < 8 and board[i][column-row+i] == 'X') :
            return True
    return False

# ---------------------------------------------------------------------

bfs_step = 1

def bfs(onTimeStates) :
    global bfs_step
    
    myBoard = onTimeStates[0][0]
    depth = onTimeStates[0][1]

    for row in range(8) :
        for column in range(8) :
            if (myBoard[row][column] != 'X') :
                continue
            if (not underThreat(myBoard, row, column)) :
                continue

            for i in range(8) :
                if (row+dy[i] < 0 or row+dy[i] > 7 or column+dx[i] < 0 or column+dx[i] > 7) :
                    continue
                if (myBoard[row+dy[i]][column+dx[i]] == 'X') :
                    continue
                
                myBFSboard = copy.deepcopy(myBoard)
                myBFSboard[row+dy[i]][column+dx[i]] = 'X'
                myBFSboard[row][column] = 'O'

                if str(myBFSboard) in visitedStates :
                    continue

                if myBFSboard in trueBoards :
                    print('found in depth ',depth+1)
                    print_gird(myBFSboard)
                    return True
                
#                 print_gird(myBoard)
#                 time.sleep(0.1)
                bfs_step += 1

                onTimeStates.append([myBFSboard, depth+1])
                visitedStates.add(str(myBFSboard))

    del onTimeStates[0]
    return False

if myBoard in trueBoards :
    print('found in depth  0')
    print_gird(myBoard)
else :
    depth = 0
    print('not found in depth ',depth)
    onTimeStates = [[myBoard, depth]]
    visitedStates = {str(myBoard)}
    while not bfs(onTimeStates) :
        if onTimeStates[0][1] > depth :
            depth += 1
            print('not found in depth ',depth)
print(bfs_step,'step')