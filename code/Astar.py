import time
import copy

# ------------------------------------------------------------------

trueBoards = []

def fill(board, size) :
    if (size == 8) :
        trueBoards.append(board.copy())
        return

    for poss in range(8) :
        fail = False
        
        for priv in range(size) :
            if (poss == board[priv]) :
                fail = True
                break
            if (abs(poss - board[priv]) == abs(size - priv)) :
                fail = True
                break
        
        if (fail) :
            continue
        
        if(size < len(board)) :
            board[size] = poss
        else :
            board.append(poss)

        fill(board, size+1)

fill([], 0)

# -------------------------------------------------------------------

trueBoard_index = 0
for trueBoard in trueBoards :
    newBoard = [['O' for i in range(8)] for j in range(8)]
    
    for row in range(8) :
        newBoard[row][ trueBoard[row] ] = 'X'
    
    trueBoards[trueBoard_index] = newBoard
    trueBoard_index += 1

# --------------------------------------------------------------------

trueBoardsSet = {""}
for trueBoard in trueBoards :
    trueBoardsSet.add(str(trueBoard))

# --------------------------------------------------------------------

def print_gird(gird) :
    print('-------------------')
    for line in gird :
        print('| ', end='')
        for square in line :
            print(square+' ', end='')
        print('|')
    print('-------------------')

# ---------------------------------------------------------------------

myBoard = [['O' for i in range(8)] for j in range(8)]

for i in range(8) :
    row,column = tuple(map(int,(input().split(','))))
    myBoard[row-1][column-1] = 'X'

print_gird(myBoard)

dx = [0, 1, 0, -1, 1, 1, -1, -1]
dy = [-1, 0, 1, 0, -1, 1, 1, -1]

# ---------------------------------------------------------------------

def underThreat(board, row, column) :
    for i in range(8) :
        if  i == column :
            continue
        if(board[row][i] == 'X') :
            return True
    for i in range(8) :
        if i == row :
            continue
        if(board[i][column] == 'X') :
            return True
        if(column+row-i >= 0 and column+row-i < 8 and board[i][column+row-i] == 'X') :
            return True
        if(column-row+i >= 0 and column-row+i < 8 and board[i][column-row+i] == 'X') :
            return True
    return False

# ---------------------------------------------------------------------

def threatCounter(board) :
    threatCount = 0
    threat_row = [0 for i in range(8)]
    threat_column = [0 for i in range(8)]
    threat_dgl = [0 for i in range(15)]
    threat_revdgl = [0 for i in range(15)]
    
    for row in range(8) :
        for column in range(8) :
            if board[row][column] != 'X' :
                continue

            threatCount += threat_row[row]
            threat_row[row] += 1

            threatCount += threat_column[column]
            threat_column[column] += 1
            
            threatCount += threat_dgl[row+column]
            threat_dgl[row+column] += 1
            
            threatCount += threat_revdgl[7+row-column]
            threat_revdgl[7+row-column] += 1

    return threatCount

# ------------------------------------------------------------------------------------

Astar_step = 1

def Astar(onTimeStates) :
    global Astar_step
    bestBoard = min(onTimeStates, key=lambda x:(x[1]+x[2]))
    myBoard = bestBoard[0]
    depth = bestBoard[1]
    
    for row in range(8) :
        for column in range(8) :
            if (myBoard[row][column] != 'X') :
                continue
            if (not underThreat(myBoard, row, column)) :
                continue

            for i in range(8) :
                if (row+dy[i] < 0 or row+dy[i] > 7 or column+dx[i] < 0 or column+dx[i] > 7) :
                    continue
                if (myBoard[row+dy[i]][column+dx[i]] == 'X') :
                    continue
                
                myAstarBoard = copy.deepcopy(myBoard)
                myAstarBoard[row+dy[i]][column+dx[i]] = 'X'
                myAstarBoard[row][column] = 'O'

                if str(myAstarBoard) in visitedStates :
                    continue

                if myAstarBoard in trueBoards :
                    print('found in depth ',depth+1)
                    print_gird(myAstarBoard)
                    return True

                # print_gird(myBoard)
                # time.sleep(0.1)
                Astar_step += 1

                onTimeStates.append([myAstarBoard, depth+1, threatCounter(myAstarBoard)])
                visitedStates.add(str(myAstarBoard))

    onTimeStates.remove(bestBoard)
    return False

if myBoard in trueBoards :
    print('found in depth  0')
    print_gird(myBoard)
else :
    depth = 0
    onTimeStates = [[myBoard, depth, threatCounter(myBoard)]]
    visitedStates = {str(myBoard)}
    while not Astar(onTimeStates) :
        pass
print(Astar_step,'step')